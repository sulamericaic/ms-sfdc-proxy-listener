FROM nexus.sulamerica.br:8000/corporativo/fis-sas:latest

MAINTAINER Carlos Rodrigues <carlos.rodrigues@sulamerica.com.br>

COPY target/*.jar /deployments/

ADD target/ms-sfdc-proxy-listener.jar ${HOME}/app.jar
ADD info.log ${HOME}/