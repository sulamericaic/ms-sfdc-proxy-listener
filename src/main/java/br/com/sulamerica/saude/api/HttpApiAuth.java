package br.com.sulamerica.saude.api;

import java.nio.charset.Charset;

import org.apache.commons.codec.binary.Base64;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class HttpApiAuth {

	private RestTemplate restTemplate;

	public HttpApiAuth() {
		restTemplate = new RestTemplate();
	}

	public Token getToken(String uri, String username, String password) {
		return restTemplate.exchange(uri, HttpMethod.POST, new HttpEntity<Token>(createHeaders(username, password)),
				Token.class).getBody();

	}

	private HttpHeaders createHeaders(String username, String password) {

		String auth = username + ":" + password;
		byte[] encodedAuth = Base64.encodeBase64(auth.getBytes(Charset.forName("US-ASCII")));
		String authHeader = "Basic " + new String(encodedAuth);
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.set("Authorization", authHeader);
		return httpHeaders;

	}
}
