package br.com.sulamerica.saude.api;

import java.net.URI;

import org.apache.camel.Body;
import org.apache.camel.Header;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

public class HttpAPI {

	private final RestTemplate restTemplate;
	private final HttpApiAuth auth;
	
	public HttpAPI(HttpApiAuth auth) {
		this.auth = auth;
		restTemplate = new RestTemplate();
	}
	public String post(@Body String payload, @Header("callback")String callback,
			@Header("auth-url") String authUrl,  @Header("username") String username, @Header("password") String password) {
		URI uri = URI.create(callback);
		HttpEntity<String> request = new HttpEntity<>(payload, headers(authUrl, username, password));
		ResponseEntity<String> response = restTemplate.postForEntity(uri, request, String.class);
		return response.getBody();
	}
	
	private HttpHeaders headers(String url, String username, String password) {
		Token token = auth.getToken(url, username, password);
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", "Bearer " + token.getBearerToken());
		headers.set("Content-type", "application/json;charset=UTF-8");
		
		return headers;
		
	}
	
	
}
