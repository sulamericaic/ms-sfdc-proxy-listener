package br.com.sulamerica.saude.camel;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;

import br.com.sulamerica.saude.api.HttpAPI;

@Component
public class CamelRoute extends RouteBuilder {

	@Value("${camel.api.path}")
	String contextPath;

	@Override
	public void configure() throws Exception {

		onException(HttpClientErrorException.class).handled(true)
			.useOriginalMessage()
			.maximumRedeliveries(5)
			.redeliveryDelay(1000);
		
		errorHandler(deadLetterChannel("activemq:queue:dead"));
		
		from("activemq:queue:myQueue?transacted=true")
			.log("Headers: ${headers}")
			.log("Body: ${body}")
			.bean(HttpAPI.class, "post");
		

	}

}
